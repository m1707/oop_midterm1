/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.midterm_1;

/**
 *
 * @author bwstx
 */
public class BMI {
    private String name;
    private double weight;
    private double height;
    
    public BMI(String name, double weight, double height){
        this.name = name;
        this.weight = weight;
        this.height = height;
    }
    
    public double calBMI(){
        return weight/((height/100)*(height/100));
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight<=0){
            System.out.println("Error : Weight must more than zero.");
        }
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        if(height<=0){
            System.out.println("Error : Height must more than zero");
        }
        this.height = height;
    }
    
    public void printLine(){
        System.out.println("----------");
    }
    
    
    public void checkBMI(){
        if(calBMI()<18.5){
            System.out.println("Underweight");
        }else if (calBMI()>=18.5 && calBMI()<=22.9){
            System.out.println("Normal");
        }else if(calBMI()>=23 && calBMI()<=24.9){
            System.out.println("Overweight");
        }else if(calBMI()>=25 && calBMI()<=29.9){
            System.out.println("Fat level 1");
        }else if(calBMI()>=30){
            System.out.println("Fat level 2");
        }
    }
}
