/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.midterm_1;

/**
 *
 * @author bwstx
 */
public class TestBMI {
    public static void main(String[] args) {
        BMI jane = new BMI("Jane",60,155);
        System.out.printf("Jane's BMI is :%.2f\n",jane.calBMI());
        jane.setHeight(160);
        System.out.printf("Jane's BMI is :%.2f\n",jane.calBMI());
        jane.checkBMI();
        jane.setWeight(0);
        System.out.printf("Jane's BMI is :%.2f\n",jane.calBMI());
        jane.checkBMI();
        jane.printLine();
        BMI beem = new BMI("Beem",50,175);
        System.out.printf("Beem's BMI is :%.2f\n",beem.calBMI());
        beem.checkBMI();
    }
    
    
}
